#SAwesome
A ScalaFX port of the JavaFX adapter for FontAwesome [1] originally written by jensd [3]

Each icon comes in different preconfigured sizes

* .default (24px)
* .small (16px)
* .big (32px)
* .large (64px)
* .huge (128px)


#Usage
```scala
new Button {
  text = "Ambulance label goes here"
  graphic = AwesomeIcon.Ambulance.default
}
```

It is important to note the .default method. It is mandatory to specify the size.

Or if you need to use a custom size for the icon, use the icon(method)

```scala
new Button {
  text = "Ambulance label goes here"
  graphic = SAwesome(SAwesomeIcon.Glass, 28)
}
```

#TODO
* Custom extension methods for creating Labeled controls
* Custom styles
* CSS integration
* Helpers for shadows, glows, etc.

Original library for JavaFX by jensd [2] [3]


[1]: http://fortawesome.github.com/Font-Awesome/
[2]: http://www.jensd.de/wordpress/?p=692
[3]: https://bitbucket.org/Jerady/fontawesomefx