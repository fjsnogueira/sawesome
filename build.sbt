
name := "SAwesome"

organization := "sawesome"

version := "0.1"

scalaVersion := "2.10.2"

assemblySettings

libraryDependencies ++= Seq(
  "org.scalafx" %% "scalafx" % "1.0.0-M4"
)

shellPrompt := { state => System.getProperty("user.name") + "> " }

// set the main class for the main 'run' task
// change Compile to Test to set it for 'test:run'
mainClass in (Compile, run) := Some("org.sawesome.Sampler")

fork in run := true